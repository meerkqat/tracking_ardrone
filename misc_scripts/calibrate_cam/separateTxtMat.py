f = open("checkers/checkers.txt", "r")
i = 1
mat = ""
for line in f:
	if not line:
		break

	if line[0] == "\n":
		continue

	if line[0] == "-":
		fout = open("checkers/c"+str(i)+".txt", "w")
		mat = mat.replace("[","")
		mat = mat.replace("]","")
		mat = mat.replace(";","")
		fout.write(mat)
		fout.close()
		i += 1
		mat = ""
		continue

	mat += line
