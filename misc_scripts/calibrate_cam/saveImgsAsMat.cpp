#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <fstream>
#include <math.h>
#include <string.h>

#include <sensor_msgs/Image.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/image_encodings.h>

#include "../../src/globalparams.h"

#define DISPLAY_IMGS false

using namespace cv;
using namespace std;
using namespace cv_bridge;

typedef const boost::shared_ptr<const sensor_msgs::Image_<std::allocator<void> > > ImageConstPtr;

ros::Subscriber sub_image;

void callback_findMarkerInImg(ImageConstPtr camImg) {
    CvImageConstPtr cvImgPtr;
    cvImgPtr = toCvCopy(camImg, sensor_msgs::image_encodings::MONO8);
    Mat imgMat = cvImgPtr->image;

    namedWindow( "img", CV_WINDOW_NORMAL ); 
    imshow("img", imgMat);
    waitKey(1);

    cerr << imgMat << endl;
    cerr << endl << "------------------------------------------------------" << endl << endl;
    ros::Duration(0.5).sleep();
    cout << "publised" << endl;

}

int main(int argc, char** argv) {
    ros::init(argc, argv, "marker_detector");
    ros::NodeHandle n;

    sub_image = n.subscribe("/ardrone/"+camera+"/image_raw", 1, callback_findMarkerInImg);

    ros::spin();
    return 0;
}