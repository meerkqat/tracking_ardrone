function mat2img(count)
	for i = 1:count
		filename = strcat('checkers/c',num2str(i));
		A = dlmread(strcat(filename, '.txt'));
		m = max(A);
		m = max(m);
		A = (A/m);
		imwrite(A,strcat(filename, '.png'));
		disp(strcat('Done ',num2str(i)))
	end
end
