#ifndef __GLOBALPARAMS_H_INCLUDED__
#define __GLOBALPARAMS_H_INCLUDED__

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

// ------------------ <EDIT ME> ---------------------

#define DO_EVAL_RUN false 		// whether to log results into text files or not
#define DO_DUMMY_RUN false 		// whether or not to send navigation commands (other than altitude fix)
								// if true, the drone won't track the marker and the built-in stabilizing should kick in

double SPEED_MODIFIER = 0.1; 	// all movement gets multiplied by this before sent to cmd_vel
double MARKER_WIDTH = 0.1; 		// used to determine current height of drone
double HOVER_HEIGHT = 1.5; 		// used as setpoint for fixing altitude

// ------------------ </EDIT ME> --------------------

std::string camera = "front";

// focal length & resloution are more or less estimates but seem to work relatively well
// in meters
double FOCAL_LENGTH = 0.003;
// in px/m
double RESOLUTION = 230400*0.823;

// in px
double RES_X = 640;
double RES_Y = 360;

double m_cam[9] = {5.9551462457963328e+02, 0., 3.3032682848115439e+02, 0., 5.9561387383040415e+02, 1.6033587079889747e+02, 0., 0., 1.};
double m_dist[5] = {-6.3109665925129221e-01, 6.5711759802663017e-01, 6.5674642001919041e-03, -1.5551230980189155e-03, -5.3136150212444400e-01};

cv::Mat camera_mat = cv::Mat(3, 3, CV_64F, m_cam);
cv::Mat distort_coef = cv::Mat(5, 1, CV_64F, m_dist);

typedef struct pid {
	// gains for proportional, integral & derivative term
	double Kp;
	double Ki;
	double Kd;

	// internal vars
	double prev_err;
	double err;
	double i;
	double i_max;
	double d;
	double dt;
	double t;

	// dampener; all outputs, that have a value=(dampen_value ± dampen_eps), will have their 
	// value reassigned to value=value*value*0.5 (effectively dampens the value because all 
	// movement is < 1.0)
	// if dampen_eps == 0 (dampen_value == arbitrary) then the dampener will have no effect
	double dampen_value;
	double dampen_eps;

	// result
	double output;

} pid_ctrl;

#endif