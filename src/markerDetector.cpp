#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <fstream>
#include <signal.h> 
#include <math.h>

#include <std_msgs/Float32.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/image_encodings.h>

#include "globalparams.h"

#define DISPLAY_IMGS_GRAY false
#define DISPLAY_IMGS_BIN true
#define DISPLAY_IMGS_SQUARES true

#define DEBUG false

using namespace cv;
using namespace std;
using namespace cv_bridge;

typedef const boost::shared_ptr<const sensor_msgs::Image_<std::allocator<void> > > ImageConstPtr;

ros::Subscriber sub_image;
ros::Publisher pub_markerLoc;
ros::Publisher pub_droneHeight;

// filters out detections of (too) small rectangles
// counts pixels
double minSquareArea = 40;
double square_side_deviation = 8;

#if DO_EVAL_RUN
	ofstream img_dist_file;
	ofstream spc_dist_file;
#endif

void sighandler(int signal) {
	if (signal == SIGINT) {
		cout << endl << "Closing files and exiting." << endl;
		#if DO_EVAL_RUN
			img_dist_file.close();
			spc_dist_file.close();
		#endif

		exit(0);
	}
}

double angle( Point pt1, Point pt2, Point pt0 ) {
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

double euclidDist(Point A, Point B) {
	return sqrt(pow(A.x-B.x, 2) + pow(A.y-B.y, 2));
}

Point center(vector<Point> quad) {
	if (quad.size() > 4) 
		cout << "Warning! vector<Point> passed to center(vector<Point>) is not a quad." << endl;
	else if (quad.size() < 4) {
		cout << "Error! vector<Point> passed to center(vector<Point>) has less than 4 points" << endl;
		exit(1);
	}

	Point pt1;
	Point pt2;
	Point mid;

	pt1.x = (quad[0].x + quad[2].x)/2.0;
	pt1.y = (quad[0].y + quad[2].y)/2.0;

	pt2.x = (quad[1].x + quad[3].x)/2.0;
	pt2.y = (quad[1].y + quad[3].y)/2.0;

	mid.x = (pt1.x + pt2.x)/2.0;
	mid.y = (pt1.y + pt2.y)/2.0;

	return mid;
}

bool approxEq(Point pt1, Point pt2) {
	// in image pixels
	double offset = 15;

	if (pt1.x-offset <= pt2.x && pt2.x <= pt1.x+offset)
		if (pt1.y-offset <= pt2.y && pt2.y <= pt1.y+offset)
			return true;
	return false;
}

void callback_findMarkerInImg(ImageConstPtr camImg) {
	double bin_threshold = 100;
	bool found = false;
	double sideLength = 0;
	double dist_from_marker_plane = 1.5000123; // (debug value - easy to see in cout)
	vector<Point> shape;
	vector<vector<Point> > contours;
	vector<vector<Point> > squares;
	vector<Point> centers; // stores all the centers of detected squares as <px, py>
	Point markerPos;

	CvImageConstPtr cvImgPtr;
	
	// - transform Image message to grayscale cv Mat image -
	cvImgPtr = toCvCopy(camImg, sensor_msgs::image_encodings::MONO8);
	Mat imgMat = cvImgPtr->image;

	// - correct lens distortion
	Mat undistortedImg;
	undistort(imgMat, undistortedImg, camera_mat, distort_coef);
	imgMat = undistortedImg;

	#if DISPLAY_IMGS_GRAY
		namedWindow( "gray", CV_WINDOW_NORMAL );
		imshow("gray", imgMat);
		waitKey(1);
	#endif

	int imgWidth = imgMat.cols;
	int imgHeight = imgMat.rows;

	// - threshold grayscale to binary -
	adaptiveThreshold(imgMat, imgMat, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 21,2);

	#if DISPLAY_IMGS_BIN
		namedWindow( "thresh", CV_WINDOW_NORMAL ); 
		imshow("thresh", imgMat);
		waitKey(1);
	#endif

	// - get all the contours from image -
	findContours(imgMat, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);

	for( size_t i = 0; i < contours.size(); i++ ) {
		// approximately estimate the shape from given contour
		approxPolyDP(Mat(contours[i]), shape, arcLength(Mat(contours[i]), true)*0.02, true);

		// looking for squares (4 corners, convex), also filter by size (lots of small false detections usually)
		if( shape.size() == 4 && isContourConvex(Mat(shape)) && fabs(contourArea(Mat(shape))) > minSquareArea ) {
			double maxCosine = 0;

			for( int j = 2; j < 5; j++ ) {
				double cosine = fabs(angle(shape[j%4], shape[j-2], shape[j-1]));
				maxCosine = MAX(maxCosine, cosine);
			}

			// check for 90 deg corners and that sides are ~ equal
			if( maxCosine < 0.3 && abs(euclidDist(shape[0],shape[1]) - euclidDist(shape[1],shape[2])) < square_side_deviation){
				squares.push_back(shape);					
			}
		}
	}

	#if DISPLAY_IMGS_SQUARES
		Mat squareConts = imgMat;
		for (int i = 0; i < imgWidth*imgHeight; i++) {
			squareConts.data[i] = 255;
		}

		for( size_t i = 0; i < squares.size(); i++ ) {
			const Point* p = &squares[i][0];
			int n = 4;
			polylines(squareConts, &p, &n, 1, true, Scalar(0,255,0), 1, 16);
		}

		namedWindow( "cont", CV_WINDOW_NORMAL ); 
		imshow("cont", squareConts);
		waitKey(1);
	#endif 

	// - find centers of detected squares, marker should be at position where 2 centers overlap -
	size_t i;
	size_t j;
	for (i = 0; i < squares.size(); i++) {
		Point c = center(squares[i]);

		// check previous centers; if overlap occurs, return c as marker position 
		// also possible (maybe TODO): check all instead of returning first best and filter for only 2 centers overlapping
		for (j = 0; j < centers.size(); j++) {
			if (approxEq(c, centers[j])) {
				found = true;
				markerPos = c;

				// sideLength is the side length of the larger square
				if (fabs(contourArea(Mat(squares[i]))) > fabs(contourArea(Mat(squares[j])))) {
					sideLength = euclidDist(squares[i][0], squares[i][1]);
				}
				else {
					sideLength = euclidDist(squares[j][0], squares[j][1]);
				}

				break;
			}
		}

		if (found) 
			break;

		centers.push_back(c);
	}

	if (!found) {
		// when the drone is too high to properly detect the marker this should kick in 
		// and detect the outer edge/square of the marker (without the inner square) as the marker
		for (i = 0; i < squares.size(); i++) {
			// todo maybe: this is kinda hacky
			if (fabs(contourArea(Mat(squares[i]))) > 400 && fabs(contourArea(Mat(squares[i]))) < 4000) {
				found = true;
				markerPos = center(squares[i]);

				sideLength = euclidDist(squares[i][0], squares[i][1]);

				break;
			}
		}
	}

	if (found) {
		cout << ".";
		cout.flush();

		#if DO_EVAL_RUN
			img_dist_file << ros::WallTime::now() << " : " << markerPos.x << " " << markerPos.y << endl;
		#endif

		#if DISPLAY_IMGS_SQUARES
			circle(squareConts, markerPos, 5, Scalar(100,100,0), -1);
			imshow("cont", squareConts);
			waitKey(1);
		#endif

		dist_from_marker_plane = ( (MARKER_WIDTH*FOCAL_LENGTH)/(sideLength/RESOLUTION) - FOCAL_LENGTH );
		#if DEBUG
			cout << "Now hovering @ " << dist_from_marker_plane << "m" << endl;
		#endif

		std_msgs::Float32 f;
		f.data = dist_from_marker_plane;
		pub_droneHeight.publish(f);

		// convert found marker from 2d image into 3d space
		double dx = ((imgHeight/2) - markerPos.y)/RESOLUTION;
		double dy = ((imgWidth/2)  - markerPos.x)/RESOLUTION;
		geometry_msgs::PointStamped pt;
		pt.header.stamp = ros::Time::now();
		pt.header.frame_id = "/ardrone_base_bottomcam";
		// position of marker in space, using ardrone local coords
		pt.point.x = dx * (dist_from_marker_plane+FOCAL_LENGTH) / FOCAL_LENGTH; 
		pt.point.y = dy * (dist_from_marker_plane+FOCAL_LENGTH) / FOCAL_LENGTH;
		pt.point.z = -dist_from_marker_plane;

		#if DO_EVAL_RUN
			spc_dist_file << ros::WallTime::now() << " : " << pt.point.x << " " << pt.point.y << " " << pt.point.z << endl;
		#endif

		pub_markerLoc.publish(pt);
	}

}

int main(int argc, char** argv) {
	ros::init(argc, argv, "marker_detector");
	ros::NodeHandle n;

	signal(SIGINT, &sighandler);

	#if DO_EVAL_RUN
		img_dist_file.open("dist_in_img.txt");
		spc_dist_file.open("dist_in_spc.txt");
	#endif

	sub_image = n.subscribe("/ardrone/"+camera+"/image_raw", 1, callback_findMarkerInImg);
	pub_markerLoc = n.advertise<geometry_msgs::PointStamped>("/marker_location", 1);
	pub_droneHeight = n.advertise<std_msgs::Float32>("/estimated_height", 1);

	ros::spin();
	return 0;
}