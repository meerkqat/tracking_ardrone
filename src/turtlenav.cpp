#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <signal.h> 
#include <math.h>

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <actionlib/client/simple_action_client.h>

#include <move_base_msgs/MoveBaseAction.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

# define PI 3.14159265358979323846
#define PART_TURN 2.0*PI/n_waypoints

using namespace std;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int n_waypoints = 4;
int n_loops = 10;
vector<move_base_msgs::MoveBaseGoal> waypoints;
vector<move_base_msgs::MoveBaseGoal>::iterator wpt_iter;

ros::Subscriber sub_waypointInit;
ros::Subscriber sub_markerPos;
ros::Subscriber sub_basePos;

ofstream drone_pos_file;
ofstream turtl_pos_file;

void sighandler(int signal) {
	if (signal == SIGINT) {
		cout << endl << "Closing!" << endl;
		drone_pos_file.close();
		turtl_pos_file.close();
		
		exit(0);
	}
}

void callback_addWaypoint (geometry_msgs::PointStamped waypt) {
	cout << "Adding waypoint..." << endl;

	move_base_msgs::MoveBaseGoal goal;
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time(0);

	goal.target_pose.pose.position.x = waypt.point.x;
	goal.target_pose.pose.position.y = waypt.point.y;
	goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(PART_TURN*waypoints.size());

	wpt_iter = waypoints.end();
	wpt_iter = waypoints.insert(wpt_iter, goal);

	cout << "Done!" << endl;
}

void callback_detected (geometry_msgs::PointStamped markerPt) {
	// write drone's position to file
	drone_pos_file  << ros::WallTime::now() << " : " << -markerPt.point.x << " " << -markerPt.point.y  << " " << -markerPt.point.z << endl;
}

void callback_basePos (geometry_msgs::PoseWithCovarianceStamped pBase) {
	// write turtle's position to file
	turtl_pos_file  << ros::WallTime::now() << " : " << pBase.pose.pose.position.x << " " << pBase.pose.pose.position.y  << " " << pBase.pose.pose.position.z << endl;
}

int main(int argc, char** argv) {
	ros::init(argc, argv, "turtlenav");
	ros::NodeHandle n;

	drone_pos_file.open("drone_global_pos.txt");
	turtl_pos_file.open("turtl_global_pos.txt");

	signal(SIGINT, &sighandler);

	// init waypoints
	cout << "Waiting for goals..." << endl;
	sub_waypointInit = n.subscribe("/clicked_point", 1, callback_addWaypoint);
	while (waypoints.size() < n_waypoints) {
		ros::spinOnce();
		ros::Duration(0.1).sleep();
	}
	sub_waypointInit.shutdown();
	cout << "Got all goals." << endl;

	sub_markerPos = n.subscribe("/marker_location", 1, callback_detected);
	sub_basePos = n.subscribe("/amcl_pose", 1, callback_basePos);

	// init action client
	cout << "Initializing action client..." << endl;
	MoveBaseClient ac("move_base", true);
	while(!ac.waitForServer(ros::Duration(5.0))){
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	int current_goal = 0;
	ac.sendGoal(waypoints[current_goal]);
	int goal_ctr = 0;

	cout << "Going into mainloop" << endl;
	ros::Rate loopRate = ros::Rate(10);
	while (ros::ok()) {
		ros::spinOnce();

		// got to waypoint, next goal
		if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
			current_goal = (current_goal+1)%n_waypoints;
			ac.sendGoal(waypoints[current_goal]);
			goal_ctr++;
		}
		// lost, resend goal
		else if (ac.getState() == actionlib::SimpleClientGoalState::RECALLED ||
				 ac.getState() == actionlib::SimpleClientGoalState::PREEMPTED ||
				 ac.getState() == actionlib::SimpleClientGoalState::ABORTED ||
				 ac.getState() == actionlib::SimpleClientGoalState::LOST ) {
			ac.sendGoal(waypoints[current_goal]);
		}

		if(goal_ctr >= n_waypoints*n_loops+1) {
			break;
		}

		loopRate.sleep();
	}

	drone_pos_file.close();
	turtl_pos_file.close();
}