#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <ardrone_autonomy/Navdata.h>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/video/tracking.hpp>

#include <iostream>
#include <fstream>
#include <string.h>
#include <signal.h> 
#include <vector>
#include <math.h>

#include <std_msgs/Empty.h>
#include <std_msgs/Float32.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Twist.h>

#include "globalparams.h"

#define DEBUG false

using namespace cv;
using namespace std;

// Ctrl+Z to land/takeoff, Ctrl+C to reset

// ====================== Tweakable params ====================== //
// how many "empty" loops to run before setting another nav goal in lost state (lostLoopLength-1 actually)
// if lostLoopLength == 1 then the drone usually does small circular motions while 
// moving in what should be the predicted direction (i.e. twist message spam to cmd_vel topic)
int lostLoopLength = 5;

// number of last seen marker locations to take into account when predicting
int nLastLocs = 10;
// First element (connection between first & second elem actually) in lastKnownLocations is 
// going to have a gain of 1. each next element is going to have a gain of nextLocGain^elemIdx
// In theory this controls how hard the drone will loop back after losing the marker - gain++ => faster turn
// if nextLocGain == 0, then simply move towards location where marker was last seen
double nextLocGain = 0.5;
// when the marker is lost, the marker prediction vector is normalized, then multiplied by this
// note: speed is clamped anyway, so not much difference for large values
double scale_prediction = 0.8;

// PID params
double Gp = 0.6;
double Gi = 0.08;
double Gd = 0.2;
double iMax = 0.2;
double pidDampEps = 0.02;

// set to 0 to disable braking
double brake_speed_modifier = 0;

// velocities in x and y direction are both multiplied by this value, then clamped
double var_speed_modifier = SPEED_MODIFIER*0.9;
// clamp max/min velocity to ±this value
double clamp_vel_at = 0.15;

// how long to search for marker after it is "lost"
ros::Duration searchDuraAfterLost = ros::Duration(8.0);
// by default the drone height estimate will come from the marker detector. However, if the drone does 
// not see the marker, this can't happen and it will have to use the proprietary navdata.altd. The drone
// will start using this sonar altd after this much time has passed and no new markers have been detected.
ros::Duration altdStatusLag = ros::Duration(1);

// ====================== Internal vars ====================== //

bool noDetectionsYet = true;
bool newDetection = false;
// makes the program print dots instead of "Found marker\n" every loop iteration
// 0 - dont stop, 1 - stop detection spam, 2 - stop lost spam
int stopSpam = 0;
int extendLostLoop = 0;

// if altitude fix is necessary during lost stage, this will account   
// for the time the drone climbs and is not looking for the marker
ros::Duration adjustSearchTForClimbT = ros::Duration(0.0);

// used for predicting in lost state
vector<geometry_msgs::PointStamped> lastKnownLocations;

// PID controller for movement in (linear) x & y directions
pid_ctrl pid_x;
pid_ctrl pid_y;

ros::Subscriber sub_markerDetector;
ros::Subscriber sub_status;
ros::Subscriber sub_heightEstimate;
ros::Publisher pub_takeoff;
ros::Publisher pub_land;
ros::Publisher pub_reset;
ros::Publisher pub_nav;

ros::CallbackQueue navdataCallQueue;
ros::NodeHandle *np; 

ardrone_autonomy::Navdata status;
std_msgs::Empty dummy;
geometry_msgs::Twist hover;
geometry_msgs::Point prev_move;

#if DO_EVAL_RUN
	ofstream detections_file;
	ofstream sonar_height_file;
	ofstream visual_height_file;
#endif

// ======================  ====================== //

// signal handler for Ctrl+Z and Ctrl+C
void sighandler(int signal) {
	if (signal == SIGTSTP) {
		if (status.state <= 2) {
			cout << "Proceeding to take off..." << endl;
			pub_takeoff.publish(dummy);

			return;
		}
		else {
			cout << endl << "Closing file and proceeding to land..." << endl;
			#if DO_EVAL_RUN
				detections_file.close();
				sonar_height_file.close();
				visual_height_file.close();
			#endif
			pub_land.publish(dummy);

			exit(0);
		}
	}
	else if (signal == SIGINT) {
		cout << endl << "Reset!" << endl;
		#if DO_EVAL_RUN
			detections_file.close();
			sonar_height_file.close();
			visual_height_file.close();
		#endif
		pub_reset.publish(dummy);
		
		exit(0);
	}
}

// initializer for PID controler
void resetPID(pid_ctrl *pid, double kp, double ki, double kd, double i_max, double dampen, double eps) {
	pid->Kp = kp;
	pid->Ki = ki;
	pid->Kd = kd;
	pid->prev_err = 0;
	pid->i = 0;
	pid->i_max = i_max; // set to 0 for no limits
	pid->dampen_value = dampen;
	pid->dampen_eps = eps;
	pid->t = ros::Time::now().toSec();
}

// updates PID controller output and internal vars to next state
void updatePID(pid_ctrl *pid, double setpoint, double observed_value) {
	double t = ros::Time::now().toSec();
	pid->dt = t - pid->t;
	pid->t = t;
	pid->err = setpoint - observed_value;
	pid->i += (pid->err*pid->dt)/2;
	pid->i = ( pid->i_max == 0 ? pid->i : min(max(pid->i, -pid->i_max), pid->i_max) ); // clamp i to ±i_max
	pid->d = (pid->err - pid->prev_err)/pid->dt;
	pid->prev_err = pid->err;

	pid->output = pid->Kp*pid->err + pid->Ki*pid->i + pid->Kd*pid->d;

	if (abs(pid->output - pid->dampen_value) < pid->dampen_eps){
		pid->output = pid->output*pid->output*0.5;
	}
}

// vector aritmetic functions
geometry_msgs::Point vector_normalize(geometry_msgs::Point v) {
	geometry_msgs::Point result;

	double magnitude = sqrt(v.x*v.x + v.y*v.y);
	result.x = ( v.x == 0 ? 0 : v.x/magnitude );
	result.y = ( v.y == 0 ? 0 : v.y/magnitude );

	return result;
}
geometry_msgs::Point vector_mul_scalar(geometry_msgs::Point v, double s) {
	geometry_msgs::Point result;
	result.x = v.x*s;
	result.y = v.y*s;

	return result;
}
geometry_msgs::Point vector_sub(geometry_msgs::Point A, geometry_msgs::Point B) {
	geometry_msgs::Point result;

	result.x = A.x-B.x;
	result.y = A.y-B.y;

	return result;
}
geometry_msgs::Point vector_add(geometry_msgs::Point A, geometry_msgs::Point B) {
	geometry_msgs::Point result;

	result.x = A.x+B.x;
	result.y = A.y+B.y;

	return result;
}
double dot_product(geometry_msgs::Point A, geometry_msgs::Point B) {

	return A.x*B.x + A.y*B.y;
}
bool isZero(geometry_msgs::Point p) {

	return (p.x == 0 && p.y == 0);
}

void fixAltd(){
	// if multiple altitude fixes are necessary during same lost state, temp accumulates adjust time
	ros::Duration temp = adjustSearchTForClimbT;
	adjustSearchTForClimbT = ros::Duration(0.0);

	double deviation = 50;// in mm
	if (status.altd >= HOVER_HEIGHT*1000 - deviation) { // m to mm conversion with some error taken into account
		return;
	}
	ros::Time start = ros::Time::now();

	#if DO_DUMMY_RUN
		pub_nav.shutdown();
		pub_nav = np->advertise<geometry_msgs::Twist>("/cmd_vel", 1);
	#endif

	pub_nav.publish(hover); // hopefully this eliminates drift
	
	// sometimes needed (especially dummy runs seem to need this)
	ros::Duration(0.5).sleep();

	cout << endl << "~ GOING UP! ~" << endl;
	geometry_msgs::Twist up;
	up.linear.z = 1.0;

	pub_nav.publish(up);

	ros::Rate climbLoopRate = ros::Rate(20);
	do {
		navdataCallQueue.callAvailable(ros::WallDuration(0));
		climbLoopRate.sleep();
	} while (status.altd < HOVER_HEIGHT*1000);
	
	pub_nav.publish(hover);

	#if DO_DUMMY_RUN
		pub_nav.shutdown();
		pub_nav = np->advertise<geometry_msgs::Twist>("/cmd_vel_dummy", 1);
	#endif

	adjustSearchTForClimbT = (ros::Time::now()-start) + temp;

	cout << "~ Fixed altitude ~" << endl;
}

// publishes vector from (0,0) towards given point (as a Twist message) to cmd_vel
// i.e. updates ardrone's movement direction using a PID controller
// also updates last "known" location for predicting marker position if marker is lost
void updateMoveDir(geometry_msgs::PointStamped pt) {
	// update last known locations
	vector<geometry_msgs::PointStamped>::iterator iter;
	iter = lastKnownLocations.begin();
	lastKnownLocations.insert(iter, pt);
	lastKnownLocations.resize(nLastLocs);

	// because pt is in local coords "observed_value" == 0 and the error == pt
	updatePID(&pid_x, pt.point.x, 0);
	updatePID(&pid_y, pt.point.y, 0);

	#if brake_speed_modifier > 0
		// "go blind" while braking
		sub_markerDetector.shutdown();

		// we just moved over the marker - brake slightly
		if (!isZero(prev_move) && pid_x.output == 0 && pid_y.output == 0) {
			geometry_msgs::Twist brake;

			brake.linear.x = -prev_move.x*brake_speed_modifier;
			brake.linear.y = -prev_move.y*brake_speed_modifier;
			brake.linear.z = 0;

			brake.angular.x = 1;
			brake.angular.y = 1;
			brake.angular.z = 0;

			pub_nav.publish(brake);
		}

		sub_markerDetector = np->subscribe("/marker_location", 1, callback_detected);
	#endif

	// generate movement message
	geometry_msgs::Twist moveDir;

	double vel_x = pid_x.output*var_speed_modifier;
	double vel_y = pid_y.output*var_speed_modifier;

	moveDir.linear.x = min(max(vel_x, -clamp_vel_at), clamp_vel_at);
	moveDir.linear.y = min(max(vel_y, -clamp_vel_at), clamp_vel_at);
	moveDir.linear.z = 0;

	prev_move.x = moveDir.linear.x;
	prev_move.y = moveDir.linear.y;

	// if drone is exactly above marker this _should_ prevent it from losing altitude
	moveDir.angular.x = 1;
	moveDir.angular.y = 1;
	moveDir.angular.z = 0;

	#if DEBUG
		if (moveDir.linear.x < 0)
			cout << endl << "Backward: " << moveDir.linear.x << "; ";
		else if (moveDir.linear.x > 0)
			cout << endl << "Forward: " << moveDir.linear.x << "; ";
		else
			cout << endl << "Hover: " << moveDir.linear.x << "; ";
		if (moveDir.linear.y < 0)
			cout << "Right: " << moveDir.linear.y << endl;
		else if (moveDir.linear.y > 0)
			cout << "Left: " << moveDir.linear.y << endl;
		else
			cout << "Hover: " << moveDir.linear.y << endl;
	#endif

	pub_nav.publish(moveDir);
}

// reads and saves current navdata
void callback_status(ardrone_autonomy::Navdata data) {
	#if DO_EVAL_RUN
		sonar_height_file << ros::WallTime::now() << " : " << data.altd << endl;
	#endif

	double alt = status.altd;
	ros::Time prev_msg_time = status.header.stamp;
	status = data;
	// only heightEstimate should move time forwards to prevent altd value getting stuck
	status.header.stamp = prev_msg_time; 

	if (ros::Time::now() - status.header.stamp < altdStatusLag) {
		status.altd = alt;
	}
}

void callback_heightEstimate(std_msgs::Float32 msg) {
	#if DO_EVAL_RUN
		visual_height_file << ros::WallTime::now() << " : " << msg.data*1000 << endl;
	#endif

	status.altd = msg.data*1000;
	status.header.stamp = ros::Time::now();
}

// called when a marker is detected - request to update movement direction
void callback_detected (geometry_msgs::PointStamped markerPt) {
	if (noDetectionsYet) { // initialize last known locations with "dummy" values
		for (int i = 0; i < nLastLocs-1; i++) { // -1 because updateMoveDir is gonna add another pt anyway
			lastKnownLocations.push_back(markerPt);
		}
	}

	updateMoveDir(markerPt);

	noDetectionsYet = false;
	newDetection = true;
}

int main(int argc, char** argv) {
	ros::init(argc, argv, "tracker");
	ros::NodeHandle n;
	ros::NodeHandle navdataNode; // so we can update navdata independent of the marker detector
	navdataNode.setCallbackQueue(&navdataCallQueue);
	np = &n;

	ros::Time lastDetectionT = ros::Time::now();

	sub_status = navdataNode.subscribe("/ardrone/navdata", 1, callback_status);
	sub_heightEstimate = navdataNode.subscribe("/estimated_height", 1, callback_heightEstimate);
	sub_markerDetector = n.subscribe("/marker_location", 1, callback_detected);
	pub_takeoff = n.advertise<std_msgs::Empty>("/ardrone/takeoff", 1);
	pub_land = n.advertise<std_msgs::Empty>("/ardrone/land", 1);
	pub_reset = n.advertise<std_msgs::Empty>("/ardrone/reset", 1);
	#if DO_DUMMY_RUN
		pub_nav = n.advertise<geometry_msgs::Twist>("/cmd_vel_dummy", 1);
	#else
		pub_nav = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
	#endif

	signal(SIGTSTP, &sighandler);
	signal(SIGINT, &sighandler);

	#if DO_EVAL_RUN
		detections_file.open("detected_per_iter.txt");
		sonar_height_file.open("sonar_height.txt");
		visual_height_file.open("visual_height.txt");
	#endif

	status.header.stamp = ros::Time::now();

	prev_move.x = 0.1;
	prev_move.y = 0.1;
	
	hover.linear.x = 0;
	hover.linear.y = 0;
	hover.linear.z = 0;
	hover.angular.x = 1; // auto-hover (all 0) tends to make the drone lose altitude (especially at low altd)
	hover.angular.y = 1;
	hover.angular.z = 0;

	geometry_msgs::Point null_vect;
	null_vect.x = 0;
	null_vect.y = 0;

	cout  << endl << "Take off with Ctrl+Z" << endl << endl;
	cout << "Land with Ctrl+Z" << endl;
	cout << "Reset with Ctrl+C" << endl;

	// loop until ardrone finished taking off
	bool airborne = false;
	int state_prev = status.state;
	while (!airborne) { 
		navdataCallQueue.callAvailable(ros::WallDuration(0));

		if (state_prev == 6 && status.state != 6)
			airborne = true;

		state_prev = status.state;
		ros::Duration(1.0).sleep();
	}

	cout << "Finished taking off." << endl;

	// initialize PID controllers
	resetPID(&pid_x, Gp,Gi,Gd, iMax, 0, pidDampEps);
	resetPID(&pid_y, Gp,Gi,Gd, iMax, 0, pidDampEps);

	// rate of mainloop in Hz
	ros::Rate loopRate = ros::Rate(10);

	while (ros::ok()) {
		navdataCallQueue.callAvailable(ros::WallDuration(0)); // navdata queue
		fixAltd();

		ros::spinOnce(); // marker detection queue

		// wait for first marker detection
		if (noDetectionsYet) continue;

		// if marker has been detected, callback will take care of updating movement direction
		if(newDetection) { // found the marker

			if (stopSpam != 1){
				cout << endl << "Detected marker.";
				cout.flush();
				stopSpam = 1;
			}
			else {
				cout << ".";
				cout.flush();
			}

			#if DO_EVAL_RUN
				detections_file << "1 - " << ros::WallTime::now() << endl;
			#endif

			lastDetectionT = ros::Time::now();
			newDetection = false;
			extendLostLoop = 0;
		}
		else { // lost the marker
			// after set time stop searching and go back to hover
			if(ros::Time::now()-lastDetectionT > searchDuraAfterLost+adjustSearchTForClimbT) {
				pub_nav.publish(hover);

				noDetectionsYet = true;
				newDetection = false;
				stopSpam = 0;
				extendLostLoop = 0;
	
				resetPID(&pid_x, Gp,Gi,Gd, iMax, 0, pidDampEps);
				resetPID(&pid_y, Gp,Gi,Gd, iMax, 0, pidDampEps);

				cout << endl << "Gave up searching for marker." << endl;

				continue;
			}
			if (stopSpam != 2){
				cout << endl << "No marker detected in this iteration.";
				cout.flush();
				stopSpam = 2;
			}
			else {
				cout << ".";
				cout.flush();
			}

			#if DO_EVAL_RUN
				detections_file << "0 - " << ros::WallTime::now() << endl;
			#endif

			if (extendLostLoop == 0) {
				// predict next marker location
				geometry_msgs::PointStamped prediction;

				// first connection is the main direction
				// all other connections have to be mirrored across this one - we assume the drone
				// is doing circular motions
				geometry_msgs::Point temp_vect = vector_mul_scalar(lastKnownLocations[0].point, 2);
				geometry_msgs::Point main_dir = vector_sub(temp_vect, lastKnownLocations[1].point);
				prediction.point = vector_add(prediction.point, main_dir);

				geometry_msgs::Point main_dir_normal = vector_normalize(main_dir);
				// iterate over all the other connections
				std::vector<geometry_msgs::PointStamped>::size_type i;
				std::vector<geometry_msgs::PointStamped>::size_type j;
				for (i = 1; i < lastKnownLocations.size()-1; i++) {
					// calculate next direction
					double gain = pow(nextLocGain, i); 
					temp_vect = vector_mul_scalar(lastKnownLocations[i].point, 2);
					geometry_msgs::Point secondary_dir = vector_sub(temp_vect, lastKnownLocations[i+1].point);

					// mirror next direction across main direction
					geometry_msgs::Point secondary_dir_neg = vector_sub(null_vect, secondary_dir);
					geometry_msgs::Point snd_term = vector_mul_scalar(main_dir_normal, 2*dot_product(main_dir_normal,secondary_dir_neg));
					geometry_msgs::Point mirrored_secondary_dir = vector_sub(secondary_dir_neg, snd_term);

					// include next direction into overall direction, normalized by its gain
					prediction.point = vector_add(prediction.point, vector_mul_scalar(mirrored_secondary_dir, gain));

				}
				// normalize, otherwise vectors are gonna get progressively larger (even with averaging)
				prediction.point = vector_mul_scalar(vector_normalize(prediction.point), scale_prediction);

				prediction.header = lastKnownLocations[0].header;
				prediction.header.stamp = ros::Time::now();

				updateMoveDir(prediction);
			}
			
			extendLostLoop++;
			extendLostLoop = extendLostLoop % lostLoopLength;
		}

		loopRate.sleep();
	}
}