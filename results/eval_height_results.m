function eval_height_results (folder_name, seconds_to_show, start_from, measureline, save_figs_to_file)
    if nargin < 5
        save_figs_to_file = true;
    end
	if nargin < 4
        measureline = 170;
    end
	if nargin < 3
        start_from = 0;
    end
    if nargin < 2
        seconds_to_show = 1000000;
    end

	% sonar dettermined height
    fileID = fopen(strcat(folder_name,'/sonar_height.txt'),'r');
    A = fscanf(fileID,'%f : %f');
    fclose(fileID);
    
    A = reshape(A, 2, [])';
    
    t_begin = A(1,1)+start_from;
    t_end = t_begin+seconds_to_show;

    idx = A(:,1) >= t_begin & A(:,1) <= t_end;
    data = A(idx,:);
    data(:,1) = data(:,1)-t_begin;

    data(:,2) = data(:,2)./10;
    data(:,1) = data(:,1)-data(1,1);

    % visually determined height
    fileID = fopen(strcat(folder_name,'/visual_height.txt'),'r');
    A = fscanf(fileID,'%f : %f');
    fclose(fileID);
    
    A = reshape(A, 2, [])';
    
    t_begin = A(1,1)+start_from;
    t_end = t_begin+seconds_to_show;

    idx = A(:,1) >= t_begin & A(:,1) <= t_end;
    data_ = A(idx,:);
    data_(:,1) = data_(:,1)-t_begin;

    data_(:,2) = data_(:,2)./10;
    data_(:,1) = data_(:,1)-data_(1,1);

    l1 = size(data, 1);
    l2 = size(data_, 1);
    l = min(l1,l2);
    data = data(1:l,:);
    data_ = data_(1:l,:);
    lim = max([max(data(:,2)),max(data_(:,2)),180]);


    % plot sonar dettermined height
    fig1 = figure(1); clf; hold on;
    %title('Ocenjena visina na podlagi sonarja')
    axis([0,data(end,1),0,lim])
    scatter(data(:,1), data(:,2), 20, 'o')
    m = mean(data(:,2));
    disp('Sonar mean = ')
    disp(m)
    line([data(1,1),data(end,1)],[m,m], 'Color', 'g')
    line([data(1,1),data(end,1)],[measureline,measureline], 'Color',  [1 0.5 0.2])
    ylabel('Ocenjena visina [cm]')
    xlabel('Cas [s]')
    hold off;
 
    if save_figs_to_file
        saveas(fig1, strcat(folder_name,'/sonar_height.png'))
    end

    % plot visually determined height
    fig2 = figure(2); clf; hold on;
    %title('Ocenjena visina na podlagi kamere')
    axis([0,data_(end,1),0,lim])
    scatter(data_(:,1), data_(:,2), 20, 'o')
    m = mean(data_(:,2));
    disp('Visual mean = ')
    disp(m)
    line([data_(1,1),data_(end,1)],[m,m], 'Color', 'g')
    line([data_(1,1),data_(end,1)],[measureline,measureline], 'Color',  [1 0.5 0.2])
    ylabel('Ocenjena visina [cm]')
    xlabel('Cas [s]')
    hold off;
 
    if save_figs_to_file
        saveas(fig2, strcat(folder_name,'/visual_height.png'))
    end
end