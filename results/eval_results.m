function eval_results (folder_name, fast_render, seconds_to_show, start_from, save_figs_to_file, n_seconds_considered_lost)
    if nargin < 6
        n_seconds_considered_lost = 1;
    end
    if nargin < 5
        save_figs_to_file = true;
    end
    if nargin < 4
        start_from = 0;
    end
    if nargin < 3
        seconds_to_show = 1000000;
    end
    if nargin < 2
        fast_render = true;
    end

    % number of detections vs number of non detections during eval
    fileID = fopen(strcat(folder_name,'/detected_per_iter.txt'),'r');
    A = fscanf(fileID,'%d - %f');
    fclose(fileID);
    
    A = reshape(A, 2, [])';

    n_pos = size(find(A(:,1) == 1));
    n_neg = size(A,1) - n_pos;
    result = [n_pos(:,1),n_neg(:,1)];
    
    fig1 = figure(1); clf; hold on;
    %title('Stevilo iteracij, kjer je in kjer ni bila zaznana oznaka')
    bar(result)
    disp('detect:nodetect ratio = ')
    disp(result(1,1)/result(1,2))
    Labels = {'Stevilo detekcij', 'Stevilo iteracij brez detekcije'};
    set(gca, 'XTick', 1:2, 'XTickLabel', Labels);
    hold off;
    
    if save_figs_to_file
        saveas(fig1, strcat(folder_name,'/detected_per_iter.png'))
    end
    
    % distance from marker in spatial coord sys
    fileID = fopen(strcat(folder_name,'/dist_in_spc.txt'),'r');
    A = fscanf(fileID,'%f : %f %f %f');
    fclose(fileID);
    A = reshape(A, 4, [])';
    A = A(:,1:3);

    t_begin = A(1,1)+start_from;
    t_end = t_begin+seconds_to_show;

    idx = A(:,1) >= t_begin & A(:,1) <= t_end;
    data = A(idx,:);
    data(:,1) = data(:,1)-t_begin;

    t1 = [0;data(:,1)];
    t2 = [data(:,1);0];
    dt = t2-t1;
    disp('#of times lost = ')
    disp(size(dt(dt(:,1)>n_seconds_considered_lost), 1))

    D  = sqrt(sum(data(:,2:3) .^ 2, 2));
    result = [data(:,1),D];

    fig2 = figure(2); clf; hold on;
    %title('Razdalja kvadrokopterja od oznake')
    axis([0,result(end,1),0,1.1])
    plot(result(:,1), result(:,2))
    m = mean(result(:,2));
    disp('Spatial mean = ')
    disp(m)
    line([result(1,1),result(end,1)],[m,m], 'Color', 'g')
    ylabel('Razdalja do oznake [m]')
    xlabel('Cas [s]')
    hold off;
 
    if save_figs_to_file
        saveas(fig2, strcat(folder_name,'/dist_in_spc.png'))
    end
    
    % scatterplot of detected points in image
    img_dim = [640 360];
    fileID = fopen(strcat(folder_name,'/dist_in_img.txt'),'r');
    A = fscanf(fileID,'%f : %f %f');
    fclose(fileID);
    
    A = reshape(A, 3, [])';
    
    t_begin = A(1,1)+start_from;
    t_end = t_begin+seconds_to_show;

    idx = A(:,1) >= t_begin & A(:,1) <= t_end;
    data = A(idx,:);
    data(:,1) = data(:,1)-t_begin;

    fig3 = figure(3); clf; hold on;
    title('Polozaji oznake na slikovni ravnini kamere')
    axis([0,img_dim(1),0,img_dim(2)])
    
    x = data(:,2)'; 
    y = data(:,3)';
    z = zeros(size(x));
    col = linspace(255,0,size(data,1));

    if (fast_render)
        scatter(x, y, 3, col)
    else
        surface([x;x],[y;y],[z;z],[col;col],'edgecol','interp','Marker','o','MarkerSize',3,'linestyle','none');
    end

    hold off;
    
    if save_figs_to_file
        saveas(fig3, strcat(folder_name,'/scatter_detects_in_img.png'))
    end

    %{
    % distance from marker in image coord sys
    fileID = fopen(strcat(folder_name,'/dist_in_img.txt'),'r');
    A = fscanf(fileID,'%f : %f %f');
    fclose(fileID);
    
    A = reshape(A, 3, [])';

    t_begin = A(1,1)+start_from;
    t_end = t_begin+seconds_to_show;    
    
    idx = A(:,1) >= t_begin & A(:,1) <= t_end;
    data = A(idx,:);
    data(:,1) = data(:,1)-t_begin;

    mat_h = size(data,1);
    center = repmat(img_dim, mat_h, 1);


    D  = sqrt(sum((data(:,2:3)-center) .^ 2, 2));
    result = [data(:,1),D];

    fig4 = figure(4); clf; hold on;
    %title('Distance from marker in image coordinate system')
    %axis([0,result(end,1),0,368])
    plot(result(:,1), result(:,2))
    m = mean(result(:,2));
    disp('Pixel mean = ')
    disp(m)
    line([result(1,1),result(end,1)],[m,m], 'Color', 'g')
    ylabel('Distance from marker [px]')
    xlabel('Time [s]')
    hold off;
    
    if save_figs_to_file
        saveas(fig4, strcat(folder_name,'/dist_in_img.png'))
    end
    %}
    
end